CREATE TABLE Book
(
    ISBN CHAR(13),
    Title VARCHAR(120) NOT NULL,
    Auther VARCHAR(120) NOT NULL,
    Publisher VARCHAR(120),
    Num_copies INT Unsigned,
    Branch_Id INT,
    PRIMARY KEY(ISBN),
    FOREIGN KEY(Branch_Id) REFERENCES Branch(Id) 
);
