CREATE TABLE Contract (
    Id INT AUTO_INCREMENT,
    Estimated_cost INT Unsigned,
    Completion_date DATE,
    Manager_id INT,
    PRIMARY KEY(Id),
    FOREIGN KEY(Manager_id) REFERENCES Manager(Id)
);