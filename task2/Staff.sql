CREATE TABLE Staff (
    Id INT AUTO_INCREMENT,
    Staff_name VARCHAR(120) NOT NULL,
    Staff_location VARCHAR(200),
    Manager_id INT,
    PRIMARY KEY(Id),
    FOREIGN KEY(Manager_id) REFERENCES Manager(Id)
);