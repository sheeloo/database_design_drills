CREATE TABLE Manager_Client (
    Manager_id INT NOT NULL,
    Client_id INT NOT NULL,
    PRIMARY KEY (Manager_id, Client_id),
    FOREIGN KEY(Manager_id) REFERENCES Manager(Id),
    FOREIGN KEY(Client_id) REFERENCES Client(Id)
);