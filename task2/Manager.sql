CREATE TABLE Manager (
    Id INT AUTO_INCREMENT,
    Manager_name VARCHAR(120) NOT NULL,
    Manager_location VARCHAR(200) NOT NULL,
    PRIMARY KEY(Id)
);