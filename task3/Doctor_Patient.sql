CREATE TABLE Doctor_Patient (
    Doctor_id INT NOT NULL,
    Patient_id INT NOT NULL,
    PRIMARY KEY (Doctor_id, Patient_id),
    FOREIGN KEY(Doctor_id) REFERENCES Manager(Id),
    FOREIGN KEY(Patient_id) REFERENCES Patient(Id)
);