CREATE TABLE Prescription (
    Id INT AUTO_INCREMENT,
    Drug_name VARCHAR(120),
    Date DATE,
    Patient_id INT,
    PRIMARY KEY(Id),
    FOREIGN KEY(Patient_id) REFERENCES Patient(Id),
    FOREIGN KEY(Drug_name) REFERENCES Drug(Name)
);