CREATE TABLE Drug (
    Name VARCHAR(120) NOT NULL,
    Dose INT Unsigned,
    Date DATE NOT NULL,
    PRIMARY KEY(Name)
);